#ifndef _OBJ_H_
#define _OBJ_H_
#include <string>

struct IObject {
	virtual ~IObject();
	virtual std::string toString() const = 0;
};

#endif