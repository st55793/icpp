#include "Integer.h"
#include <string>
using namespace std;

Integer::Integer(int value)
{
	this->value = value;
}

std::string Integer::toString() const
{
	return to_string(value);
}
