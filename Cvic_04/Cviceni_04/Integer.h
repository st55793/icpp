#ifndef _INT_H_
#define _INT_H_
#include "IObject.h"

struct Integer : IObject {
public:
	Integer(int value);
	virtual std::string toString() const override;
private:
	int value;
};

#endif