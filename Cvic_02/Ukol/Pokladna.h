#ifndef __POKLADNA_H
#define __POKLADNA_H

#include "Uctenka.h"

class Pokladna {
private:
	Uctenka* uctenky;
	int pocetVydanychUctenek;
	static int citacId;
public:
	Pokladna();
	~Pokladna();
	Uctenka& vystavUctenku(double castka, double dph);
	Uctenka& dejUctenku(int id);
	double dejCastku() const;
	double dejCastkuVcDph() const;
};

#endif
