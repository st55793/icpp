#include "Pokladna.h"

Pokladna::Pokladna()
{
	this->uctenky = new Uctenka[10];
	this->pocetVydanychUctenek = 0;
}

Pokladna::~Pokladna()
{
	delete[] uctenky;
}

int Pokladna::citacId = 1000;

Uctenka& Pokladna::vystavUctenku(double castka, double dph)
{
	uctenky[pocetVydanychUctenek].setCisloUctenky(citacId++);
	uctenky[pocetVydanychUctenek].setCastka(castka);
	uctenky[pocetVydanychUctenek].setDph(dph);
	pocetVydanychUctenek++;
	return uctenky[pocetVydanychUctenek - 1];
}

Uctenka& Pokladna::dejUctenku(int id)
{
	for (int i = 0; i < 10; i++)
	{
		if (uctenky->getCisloUctenky())
		{
			return uctenky[i];
		}
	}
	return uctenky[0];
}

double Pokladna::dejCastku() const
{
	double celkem = 0;
	for (int i = 0; i < 10; i++)
	{
		celkem += uctenky[i].getCastka();
	}
	return celkem;
}

double Pokladna::dejCastkuVcDph() const
{
	double celkem = 0;
	for (int i = 0; i < 10; i++)
	{
		celkem += uctenky[i].getCastka() * (1 + uctenky[i].getDph());
	}
	return celkem;
}