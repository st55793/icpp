#include<iostream>
#include<fstream>
#include "Osoba.h"

using namespace std;

void uloz();
void nacti();
void ulozBin();
void nactiBin();

int main() {
	cout << "Text file:" << endl;
	uloz();
	nacti();
	cout << "Binary file:" << endl;
	ulozBin();
	nactiBin();

	cin.get();
	return 0;
}

void uloz()
{
	Adresa adresa = Adresa{ "Matejova", "Matejovice", 7 };
	Datum datum = Datum{ 7, 7, 2002 };
	const int pocet = 3;
	Osoba osoby[pocet];
	osoby[0] = Osoba{ "Matej", "Matejovic", adresa, datum };
	osoby[1] = Osoba{ "Matej2", "Matejovic2", adresa, datum };
	osoby[2] = Osoba{ "Matej3", "Matejovic3", adresa, datum };
	ofstream out{};
	out.open("osoby.txt");

	if (out.is_open())
	{
		out << pocet << endl;
		for (int i = 0; i < pocet; i++)
		{
			out << osoby[i] << endl;
		}
		out.close();
	}
	else
	{
		cerr << "Soubor se nepodarilo otevrit.";
	}
}

void nacti()
{
	int pocet = 0;
	ifstream in{};
	in.open("osoby.txt");
	if (in.is_open())
	{
		in >> pocet;
	}
	else
	{
		cerr << "Soubor se nepodarilo otevrit.";
	}
	Osoba* osoby = new Osoba[pocet];
	if (in.is_open())
	{
		for (int i = 0; i < pocet; i++)
		{
			in >> osoby[i];
		}
	}
	else
	{
		cerr << "Soubor se nepodarilo otevrit.";
	}
	for (int i = 0; i < pocet; i++)
	{
		cout << osoby[i] << endl;
	}
	delete[] osoby;
	in.close();
}

void ulozBin()
{
	Adresa adresa = Adresa{ "Matejova", "Matejovice", 7 };
	Datum datum = Datum{ 7, 7, 2002 };
	const int pocet = 3;
	Osoba osoby[pocet];
	osoby[0] = Osoba{ "Matej", "Matejovic", adresa, datum };
	osoby[1] = Osoba{ "Matej2", "Matejovic2", adresa, datum };
	osoby[2] = Osoba{ "Matej3", "Matejovic3", adresa, datum };
	ofstream out{};
	int num = 0;
	out.open("osoby.bin", ios::out | ios::binary);
	if (out.is_open())
	{
		out.write((const char*)& pocet, sizeof(pocet));
		for (int i = 0; i < pocet; i++)
		{
			out.write(osoby[i].getJmeno().c_str(), osoby[i].getJmeno().size() + 1);
			out.write(osoby[i].getPrijmeni().c_str(), osoby[i].getPrijmeni().size() + 1);
			out.write(osoby[i].getTrvaleBydliste().getUlice().c_str(), osoby[i].getTrvaleBydliste().getUlice().size() + 1);
			out.write(osoby[i].getTrvaleBydliste().getMesto().c_str(), osoby[i].getTrvaleBydliste().getMesto().size() + 1);
			num = osoby[i].getTrvaleBydliste().getPsc();
			out.write((const char*)& num, sizeof(num));
			num = osoby[i].getDatumNarozeni().getDen();
			out.write((const char*)& num, sizeof(num));
			num = osoby[i].getDatumNarozeni().getMesic();
			out.write((const char*)& num, sizeof(num));
			num = osoby[i].getDatumNarozeni().getRok();
			out.write((const char*)& num, sizeof(num));
		}
		out.close();
	}
	else
	{
		cerr << "Soubor se nepodarilo otevrit.";
	}
}

void nactiBin()
{
	int pocet = 0;
	ifstream in{};
	in.open("osoby.bin", ios::in | ios::binary);
	if (in.is_open())
	{
		in.read((char*)& pocet, sizeof(pocet));
	}
	else
	{
		cerr << "Soubor se nepodarilo otevrit.";
	}
	Osoba* osoby = new Osoba[pocet];
	string str{};
	int num = 0;
	Adresa adresa = Adresa{};
	Datum datum = Datum{};
	if (in.is_open())
	{
		for (int i = 0; i < pocet; i++) {
			getline(in, str, '\0');
			osoby[i].setJmeno(str);
			getline(in, str, '\0');
			osoby[i].setPrijmeni(str);
			getline(in, str, '\0');
			adresa.setUlice(str);
			getline(in, str, '\0');
			adresa.setMesto(str);
			in.read((char*)& num, sizeof(pocet));
			adresa.setPsc(num);
			osoby[i].setTrvaleBydliste(adresa);
			in.read((char*)& num, sizeof(pocet));
			datum.setDen(num);
			in.read((char*)& num, sizeof(pocet));
			datum.setMesic(num);
			in.read((char*)& num, sizeof(pocet));
			datum.setRok(num);
			osoby[i].setDatumNarozeni(datum);
		}
	}
	else
	{
		cerr << "Soubor se nepodarilo otevrit.";
	}
	for (int i = 0; i < pocet; i++)
	{
		cout << osoby[i] << endl;
	}
	delete[] osoby;
	in.close();
}
