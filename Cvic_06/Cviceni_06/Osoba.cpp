#include "Osoba.h"

Osoba::Osoba()
{
}

Osoba::Osoba(std::string jmeno, std::string prijmeni, Adresa trvaleBydliste, Datum datumNarozeni)
{
	this->_jmeno = jmeno;
	this->_prijmeni = prijmeni;
	this->_trvaleBydliste = trvaleBydliste;
	this->_datumNarozeni = datumNarozeni;
}

Osoba::~Osoba()
{
}

void Osoba::setJmeno(std::string jmeno)
{
	this->_jmeno = jmeno;
}

void Osoba::setPrijmeni(std::string prijmeni)
{
	this->_prijmeni = prijmeni;
}

void Osoba::setTrvaleBydliste(Adresa trvaleBydliste)
{
	this->_trvaleBydliste = trvaleBydliste;
}

void Osoba::setDatumNarozeni(Datum datumNarozeni)
{
	this->_datumNarozeni = datumNarozeni;
}

std::string Osoba::getJmeno() const
{
	return this->_jmeno;
}

std::string Osoba::getPrijmeni() const
{
	return this->_prijmeni;
}

Adresa Osoba::getTrvaleBydliste() const
{
	return this->_trvaleBydliste;
}

Datum Osoba::getDatumNarozeni() const
{
	return this->_datumNarozeni;
}

std::ostream & operator<<(std::ostream & os, const Osoba & osoba)
{
	os << osoba.getJmeno() << " " << osoba.getPrijmeni() << " " << osoba.getTrvaleBydliste() << " " << osoba.getDatumNarozeni();
	return os;
}

std::istream & operator>>(std::istream & is, Osoba & osoba)
{
	std::string jmeno, prijmeni;
	Adresa trvaleBydliste;
	Datum datumNarozeni;
	is >> jmeno;
	osoba.setJmeno(jmeno);
	is >> prijmeni;
	osoba.setPrijmeni(prijmeni);
	is >> trvaleBydliste;
	osoba.setTrvaleBydliste(trvaleBydliste);
	is >> datumNarozeni;
	osoba.setDatumNarozeni(datumNarozeni);
	return is;
}
