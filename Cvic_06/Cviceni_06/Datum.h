#ifndef __DATUM_H
#define __DATUM_H

#include<iostream>

struct Datum {
private:
	int _den;
	int _mesic;
	int _rok;
public:
	Datum();
	Datum(int den, int mesic, int rok);
	~Datum();
	void setDen(int den);
	void setMesic(int mesic);
	void setRok(int rok);
	int getDen() const;
	int getMesic() const;
	int getRok() const;
};

std::ostream& operator<<(std::ostream& os, const Datum& datum);
std::istream& operator>>(std::istream& is, Datum& datum);

#endif