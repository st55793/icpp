#include "Osoba.h"

Entity::Osoba::Osoba()
{
}

Entity::Osoba::Osoba(int id, std::string jmeno, std::string telefon)
{
	this->id = id;
	this->jmeno = jmeno;
	this->telefon = telefon;
}

Entity::Osoba::~Osoba()
{
}

int Entity::Osoba::getId() const
{
	return this->id;
}

std::string Entity::Osoba::getJmeno() const
{
	return this->jmeno;
}

std::string Entity::Osoba::getTelefon() const
{
	return this->telefon;
}
