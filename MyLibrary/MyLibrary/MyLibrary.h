#ifndef MY_LIB_H
#define MY_LIB_H

#ifdef MYLIB_EXP
#define API_MYLIB_LIB __declspec(dllexport)
#else
#define API_MYLIB_LIB __declspec(dllimport)
#endif // 

class API_MYLIB_LIB MyLibrary
{
public:
	void SayHello() const;
	void SayGoodbye() const;
private:

};


#endif // !MY_LIB_H

