#include <iostream>
#include "Db.h"

using namespace std;

int main() {
	Db* db = Db::open("Cars");

	auto idType = Db::Field("id", FieldType::Integer);
	auto carName = Db::Field("name", FieldType::String);
	auto idM = Db::Field("idmaker", FieldType::Integer);
	auto carFields = combineToDefinition(idType, carName, idM);
	Table* cars = db->openOrCreateTable("types", 3, carFields);

	auto idMaker = Db::Field("id", FieldType::Integer);
	auto makerName = Db::Field("name", FieldType::String);
	auto makerFields = combineToDefinition(idMaker, makerName);
	Table* makers = db->openOrCreateTable("makers", 2, makerFields);

	/*makers->insert(combineToRow(Db::Int(1), Db::String("Skoda")));
	cars->insert(combineToRow(Db::Int(1), Db::String("Octavia"), Db::Int(1)));
	makers->insert(combineToRow(Db::Int(2), Db::String("Volkswagen")));
	cars->insert(combineToRow(Db::Int(2), Db::String("Passat"), Db::Int(2)));
	makers->insert(combineToRow(Db::Int(3), Db::String("Renault")));
	cars->insert(combineToRow(Db::Int(3), Db::String("Megane"), Db::Int(3)));
	makers->insert(combineToRow(Db::Int(4), Db::String("Seat")));
	cars->insert(combineToRow(Db::Int(4), Db::String("Leon"), Db::Int(4)));
	makers->insert(combineToRow(Db::Int(5), Db::String("Toyota")));
	cars->insert(combineToRow(Db::Int(5), Db::String("Rav4"), Db::Int(5)));
	*/
	cars->commit();
	makers->commit();


	int vyber;
	int id;
	string nazev;
	int idMak;

	while (true)
	{
		cout << "-----MENU-----" << endl;
		cout << "1) Vypis tabulek" << endl;
		cout << "2) Pridani zaznamu" << endl;
		cout << "3) Odstraneni zaznamu" << endl;
		cout << "4) Uprava zaznamu" << endl;
		cout << "5) Zapsani zaznamu do souboru" << endl;
		cout << "0) Vypnuti programu" << endl;
		cout << "-------------" << endl;
		cout << "Tvuj vyber: ";
		cin >> vyber;
		if (vyber == 0)
		{
			break;
		}
		else
		{
			if (vyber == 1) // v�pis
			{
				cout << "-----Vypis z obou tabulek-----" << endl;
				auto it = cars->select();

				while (it->moveNext())
				{
					auto row = it->getRow();
					Object** data = makers->ziskejRadek(row[2]->getInt());
					cout << row[0]->getInt() << " " << data[1]->getString() << " " << row[1]->getString() << endl;
				}
				it->close();
			}
			else
			{
				if (vyber == 2) // p�id�v�n�
				{
					cout << "-----Vyber tabulku kam bude p�id�vat-----" << endl;
					cout << "1) Znacky aut" << endl;
					cout << "2) Modely" << endl;
					cout << "Pro konec zmackni cokoli jineho" << endl;
					cout << "-------------" << endl;
					cout << "Tvuj vyber: ";
					cin >> vyber;
					if (vyber == 1) // Znacky
					{
						cout << "Zadej Id: ";
						cin >> id;
						cout << "Zadej nazev znacky: ";
						cin >> nazev;
						makers->insert(combineToRow(Db::Int(id), Db::String(nazev)));
						cout << "Vlozeno" << endl;
					}
					else
					{
						if (vyber == 2) // Modely
						{
							cout << "Zadej Id: ";
							cin >> id;
							cout << "Zadej nazev modelu: ";
							cin >> nazev;
							cout << "-----Vyber automobilky-----" << endl;
							auto it = makers->select();

							while (it->moveNext())
							{
								auto row = it->getRow();
								cout << row[0]->getInt() << " " << row[1]->getString() << endl;
							}
							it->close();
							cout << "Zadej id znacky (pro konec zmackni 0): ";
							cin >> idMak;
							if (idMak > 0)
							{
								cars->insert(combineToRow(Db::Int(id), Db::String(nazev), Db::Int(idMak)));
								cout << "Vlozeno" << endl;
							}
						}
					}
				}
				else
				{
					if (vyber == 3) // odstran�n�
					{
						cout << "-----Vyber tabulku kde budeme mazat-----" << endl;
						cout << "1) Znacky aut" << endl;
						cout << "2) Modely" << endl;
						cout << "Pro konec zmackni cokoli jineho" << endl;
						cout << "-------------" << endl;
						cout << "Tvuj vyber: ";
						cin >> vyber;
						if (vyber == 1) // Znacky
						{
							cout << "-----Vyber Znacky k vymazani-----" << endl;
							auto it = makers->select();
							int radek = 0;
							while (it->moveNext())
							{
								auto row = it->getRow();
								cout << radek << " " << row[1]->getString() << endl;
								radek++;
							}
							it->close();
							cout << "-----UPOZORNENI - Pokud vymazete znacku budou vymazany i prislusne modely-----" << endl;
							cout << "Zadej radek ktery chces vymazat (Pro konec zadej " << radek << "): ";
							cin >> id;
							if (radek != id) {
								Object** data = makers->ziskejRadekPodlePoctu(id);
								int pocet = cars->getRowCount();
								for (int i = 0; i < pocet; i++)
								{
									Object** r = cars->ziskejRadekPodlePoctu(i);
									if (data[0]->getInt() == r[2]->getInt())
									{
										cars->remove(i);
										i--;
										pocet--;
									}
								}
								makers->remove(id);
								cout << "Odstraneno" << endl;
							}
						}
						else
						{
							if (vyber == 2) // Modely
							{
								cout << "-----Vyber Modelu k vymazani-----" << endl;
								auto it = cars->select();
								int radek = 0;
								while (it->moveNext())
								{
									auto row = it->getRow();
									Object** data = makers->ziskejRadek(row[2]->getInt());
									cout << radek << " " << data[1]->getString() << " " << row[1]->getString() << endl;
									radek++;
								}
								it->close();
								cout << "Zadej radek ktery chces vymazat (Pro konec zadej "<< radek << "): ";
								cin >> id;
								if (radek != id) {

									cars->remove(id);

									cout << "Odstraneno" << endl;
								}
							}
						}
					}
					else
					{
						if (vyber == 4) // �prava
						{
							cout << "-----Vyber tabulku kde budeme upravovat-----" << endl;
							cout << "1) Znacky aut" << endl;
							cout << "2) Modely" << endl;
							cout << "Pro konec zmackni cokoli jineho" << endl;
							cout << "-------------" << endl;
							cout << "Tvuj vyber: ";
							cin >> vyber;
							if (vyber == 1) // Znacky
							{
								cout << "-----Vyber znacky k uprave-----" << endl;
								auto it = makers->select();
								int radek = 0;
								while (it->moveNext())
								{
									auto row = it->getRow();
									cout << radek << " " << row[1]->getString() << endl;
									radek++;
								}
								cout << "Zadej radek ktery chces upravit (Pro konec zadej " << radek << "): ";
								cin >> id;
								if (radek != id) {
									Object** u = makers->ziskejRadekPodlePoctu(id);
									cout << "Zadej Id (puvodni id:" << u[0]->getInt() << ")";
									cin >> id;
									cout << "Zadej nazev znacky (puvodni nazev:" << u[1]->getString() << ")";
									cin >> nazev;
									u[0]->setInt(id);
									u[1]->setString(nazev);
									cout << "Upraveno" << endl;
								}
							}
							else
							{
								if (vyber == 2) // Modely
								{
									cout << "-----Vyber Modelu k uprave-----" << endl;
									auto it = cars->select();
									int radek = 0;
									while (it->moveNext())
									{
										auto row = it->getRow();
										Object** data = makers->ziskejRadek(row[2]->getInt());
										cout << radek << " " << data[1]->getString() << " " << row[1]->getString() << endl;
										radek++;
									}
									it->close();
									cout << "Zadej radek ktery chces upravit (Pro konec zadej " << radek << "): ";
									cin >> id;
									if (radek != id) {
										Object** u = cars->ziskejRadekPodlePoctu(id);
										cout << "Zadej Id (puvodni id:" << u[0]->getInt() << ")";
										cin >> id;
										cout << "Zadej nazev modelu (puvodni nazev:" << u[1]->getString() << ")";
										cin >> nazev;
										cout << "-----Vyber idcka znacky-----" << endl;
										auto it = makers->select();
										while (it->moveNext())
										{
											auto row = it->getRow();
											cout << row[0]->getInt() << " " << row[1]->getString() << endl;
										}
										cout << "Zadej id vyrobce (puvodni id:" << u[2]->getInt() << ")";
										cin >> idMak;
										u[0]->setInt(id);
										u[1]->setString(nazev);
										u[2]->setInt(idMak);
										cout << "Upraveno" << endl;
									}
								}
							}
						}
						else
						{
							if (vyber == 5) // Z�pis
							{
								cars->commit();
								makers->commit();
								cout << "Zapsano!" << endl;
							}
							else
							{
								cout << "Spatny vstup. Oprav se!" << endl;
							}
						}
					}
				}
			}
		}
	}

	cars->close();
	makers->close();
	db->close();

	_CrtDumpMemoryLeaks();

	cin.get();
	return 0;
}