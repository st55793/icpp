#include "IntObject.h"

int IntObject::getInt() const
{
	return this->value;
}

void IntObject::setInt(int value)
{
	this->value = value;
}

bool IntObject::isType(FieldType type) const
{
	if (type == FieldType::Integer)
	{
		return true;
	}
	else
	{
		return false;
	}
}
