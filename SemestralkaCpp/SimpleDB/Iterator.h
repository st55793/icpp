#ifndef __MYITERATOR_H
#define __MYITERATOR_H

#include "platform.h"
#include "Object.h"
#include "IIterator.h"

class DLL_SPEC Iterator : public IIterator {
private:
	int aktualni, pocetRadku;
	Object*** data;
public:	
	Iterator(Object*** data, int pocetRadku);
	virtual ~Iterator();
	virtual bool moveNext() override;
	virtual Object** getRow() override;
	virtual int getRowId() override;
	virtual void close() override;
};

#endif
