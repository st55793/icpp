#include "DoubleObject.h"

double DoubleObject::getDouble() const
{
	return this->value;
}

void DoubleObject::setDouble(double value)
{
	this->value = value;
}

bool DoubleObject::isType(FieldType type) const
{
	if (type == FieldType::Double)
	{
		return true;
	}
	else
	{
		return false;
	}
}
