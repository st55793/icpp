#ifndef __DOUBLEOBJECT_H
#define __DOUBLEOBJECT_H

#include "Object.h"

class DLL_SPEC DoubleObject : public Object {
private:
	double value;
public:
	DoubleObject() : value(0.0) {}
	DoubleObject(double v) : value(v) {}
	virtual ~DoubleObject() {}
	virtual double getDouble() const override;
	virtual void setDouble(double value) override;
	virtual bool isType(FieldType type) const override;
};

#endif
