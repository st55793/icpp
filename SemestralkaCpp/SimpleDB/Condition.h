#ifndef __CONDITION_H
#define __CONDITION_H

#include "platform.h"

// Rozhraní definující podmínku – pouze pro bonusové metody
class DLL_SPEC Condition {
	virtual ~Condition() {}
	virtual bool matches(int fieldCount, FieldObject** fields, Object** row) const = 0;
};

#endif
