#ifndef __FIELDOBJECT_H
#define __FIELDOBJECT_H

#include "Object.h"

// Objekt popisuj�c� sloupe�ek �field�
class DLL_SPEC FieldObject : public Object {
private:
	std::string name;
	FieldType type;
public:
	FieldObject() {}
	FieldObject(std::string name, FieldType type) :name(name), type(type) {}
	virtual ~FieldObject() {}

	virtual bool isType(FieldType type) const override;

	// N�zev sloupe�ku
	std::string getName() const { return name; }
	// Typ sloupe�ku
	FieldType getType() const { return type; }
};

#endif
