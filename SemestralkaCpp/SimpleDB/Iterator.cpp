#include "Iterator.h"

Iterator::Iterator(Object*** data, int pocetRadku)
{
	this->data = data;
	this->pocetRadku = pocetRadku;
	aktualni = -1;
}

Iterator::~Iterator()
{
}

bool Iterator::moveNext()
{
	aktualni++;
	if (aktualni < pocetRadku)
	{
		return true;
	}
	else
	{
		return false;
	}
}

Object** Iterator::getRow()
{
	return data[aktualni];
}

int Iterator::getRowId()
{
	return aktualni;
}

void Iterator::close()
{
	delete this;
}
