#ifndef __INTOBJECT_H
#define __INTOBJECT_H

#include "Object.h"

class DLL_SPEC IntObject : public Object {
private:
	int value;
public:
	IntObject() : value(0) {}
	IntObject(int v) : value(v) {}
	virtual ~IntObject() {}
	virtual int getInt() const override;
	virtual void setInt(int value) override;
	virtual bool isType(FieldType type) const override;
};

#endif
