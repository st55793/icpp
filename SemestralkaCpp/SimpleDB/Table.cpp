#include "Table.h"
#include <iostream>
#include <fstream>

using namespace std;

void Table::navysKapacitu()
{
	int velikostPom = velikost * 2;
	Object*** pom = new Object * *[velikostPom];
	for (int i = 0; i < velikost; i++)
	{
		pom[i] = data[i];
	}
	for (int i = velikost; i < velikostPom; i++)
	{
		pom[i] = new Object * [pocetSloupcu];
	}
	delete[] data;
	data = pom;
	velikost = velikostPom;
}

Table::Table(std::string jmeno, std::string databaze, int pocetSloupcu, FieldObject** fields)
{
	velikost = 5;
	pocetRadku = 0;
	this->fields = fields;
	this->jmeno = jmeno;
	this->databaze = databaze;
	this->pocetSloupcu = pocetSloupcu;
	data = new Object * *[velikost];
	for (int i = 0; i < velikost; i++)
	{
		data[i] = new Object * [pocetSloupcu];
	}
}

Table::~Table()
{
	for (int i = 0; i < velikost; i++)
	{
		if (i < pocetRadku)
		{
			for (int j = 0; j < pocetSloupcu; j++)
			{
				delete data[i][j];
			}
		}
		delete[] data[i];
	}
	delete[] data;
	for (int i = 0; i < pocetSloupcu; i++)
	{
		delete fields[i];
	}
	delete[] fields;
}

void Table::insert(Object** row)
{
	if (velikost == pocetRadku)
	{
		navysKapacitu();
	}
	for (int i = 0; i < pocetSloupcu; i++)
	{
		data[pocetRadku][i] = row[i];
	}
	delete row;
	pocetRadku++;
}

void Table::remove(int rowid)
{
	for (int i = 0; i < pocetSloupcu; i++)
	{
		delete data[rowid][i];
	}
	delete[] data[rowid];
	for (int i = rowid; i < velikost; i++)
	{
		data[i] = data[i + 1];
	}
	data[velikost - 1] = new Object * [pocetSloupcu];
	pocetRadku--;
}

Iterator* Table::select()
{
	return new Iterator(data, pocetRadku);
}

void Table::commit()
{
	ofstream vystup{ "../SimpleDB/database/" + databaze + "X" + jmeno + "Xdata.txt"};
	if (vystup.is_open())
	{
		vystup << pocetRadku << endl;
		for (int i = 0; i < pocetRadku; i++)
		{
			for (int j = 0; j < pocetSloupcu; j++)
			{
				if (data[i][j]->isType(FieldType::String))
				{
					vystup << data[i][j]->getString();
				}
				else if (data[i][j]->isType(FieldType::Integer))
				{
					vystup << data[i][j]->getInt();
				}
				else if (data[i][j]->isType(FieldType::Double))
				{
					vystup << data[i][j]->getDouble();
				}
				vystup << " ";
			}
			vystup << endl;
		}
	}
	vystup.close();
}

void Table::close()
{
	delete this;
}

int Table::getRowCount() const
{
	return this->pocetRadku;
}

FieldObject** Table::getFields() const
{
	return this->fields;
}

int Table::getFieldCount() const
{
	return this->pocetSloupcu;
}

Object** Table::ziskejRadek(int id)
{
	for (int i = 0; i < pocetRadku; i++)
	{
		if (data[i][0]->getInt() == id)
		{
			return data[i];
		}
	}
	throw std::invalid_argument{ "No match with this id" };
}

Object** Table::ziskejRadekPodlePoctu(int id)
{
	return data[id];
}
