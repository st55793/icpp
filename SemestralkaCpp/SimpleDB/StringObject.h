#ifndef __STRINGOBJECT_H
#define __STRINGOBJECT_H

#include "Object.h"

class DLL_SPEC StringObject : public Object {
private:
	std::string value;
public:
	StringObject() : value("") {}
	StringObject(std::string v) : value(v) {}
	virtual ~StringObject() {}
	virtual std::string getString() const override;
	virtual void setString(std::string value) override;
	virtual bool isType(FieldType type) const override;
};

#endif
