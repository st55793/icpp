#include "Db.h"
#include <fstream>
#include <iostream>

using namespace std;

Db::Db(std::string jmeno)
{
	pocetTabulek = 0;
	this->jmeno = jmeno;
}

Db::~Db()
{
}

bool Db::jeTabulkaAktivni(string name)
{
	for (int i = 0; i < pocetTabulek; i++)
	{
		if (name.compare(aktivniTabulky[i]) == 0)
		{
			return true;
		}
	}
	return false;
}

Db* Db::open(std::string database)
{
	Db* newDb = new Db{ database };
	ifstream vstup{ "../SimpleDB/database/" + newDb->jmeno + "Xdatabase.txt" };
	if (vstup.is_open())
	{
		string str;
		while (vstup >> str)
		{
			newDb->aktivniTabulky.push_back(str);
			newDb->pocetTabulek++;
		}
		vstup.close();
		return newDb;
	}
	else
	{
		return newDb;
	}
}

void Db::close()
{
	delete this;
}

Table* Db::createTable(std::string name, int fieldsCount, FieldObject** fields)
{
	if (jeTabulkaAktivni(name))
	{
		throw exception{ "Tato tabulka jiz existuje" };
	}
	else
	{
		Table* table = new Table{ name, jmeno, fieldsCount, fields };
		pocetTabulek++;
		aktivniTabulky.push_back(name);
		ofstream vystup{ "../SimpleDB/database/" + jmeno + "Xdatabase.txt" };
		if (vystup.is_open())
		{
			for (int i = 0; i < pocetTabulek; i++)
			{
				vystup << aktivniTabulky[i] << endl;
			}
		}
		vystup.close();
		vystup.open("../SimpleDB/database/" + jmeno + "X" + name + "Xschema.txt");
		if (vystup.is_open())
		{
			vystup << fieldsCount << endl;
			for (int i = 0; i < fieldsCount; i++)
			{
				vystup << fields[i]->getName() << " " << prevedNaString(fields[i]->getType()) << endl;
			}
		}
		vystup.close();
		return table;
	}
}

Table* Db::openTable(std::string name)
{
	if (!jeTabulkaAktivni(name))
	{
		throw exception{ "Tato tabulka neexistuje" };
	}
	else
	{
		int pocetSloupcu;
		string nazev, type;
		Table* table;
		FieldObject** fields;
		ifstream vstup{ "../SimpleDB/database/" + jmeno + "X" + name + "Xschema.txt" };
		if (vstup.is_open())
		{
			vstup >> pocetSloupcu;
			fields = new FieldObject * [pocetSloupcu];
			for (int i = 0; i < pocetSloupcu; i++)
			{
				vstup >> nazev >> type;
				fields[i] = new FieldObject{ nazev, prevedNaFieldType(type) };
			}
			table = new Table{ name, jmeno, pocetSloupcu, fields };
		}
		else
		{
			throw exception{ "Soubor se nepodarilo otevrit" };
		}
		vstup.close();
		vstup.open("../SimpleDB/database/" + jmeno + "X" + name + "Xdata.txt");
		if (vstup.is_open())
		{
			int pocetRadku = 0;
			int intPom;
			double doublePom;
			string stringPom;
			string type;
			vstup >> pocetRadku;
			for (int i = 0; i < pocetRadku; i++)
			{
				Object** radek = new Object * [pocetSloupcu];
				for (int j = 0; j < pocetSloupcu; j++)
				{
					switch (fields[j]->getType())
					{
					case FieldType::String:
						vstup >> stringPom;
						radek[j] = String(stringPom);
						break;
					case FieldType::Integer:
						vstup >> intPom;
						radek[j] = Int(intPom);
						break;
					case FieldType::Double:
						vstup >> doublePom;
						radek[j] = Double(doublePom);
						break;
					default:
						break;
					}
				}
				table->insert(radek);
			}
		}
		vstup.close();
		return table;
	}
}

Table* Db::openOrCreateTable(std::string name, int fieldsCount, FieldObject** fields)
{
	if (jeTabulkaAktivni(name))
	{
		for (int i = 0; i < fieldsCount; i++)
		{
			delete fields[i];
		}
		delete[] fields;
		return openTable(name);
	}
	else
	{
		return createTable(name, fieldsCount, fields);
	}
}

Object* Db::Int(int value)
{
	return new IntObject{ value };
}

Object* Db::Double(double value)
{
	return new DoubleObject{ value };
}

Object* Db::String(std::string value)
{
	return new StringObject{ value };
}

FieldObject* Db::Field(std::string name, FieldType type)
{
	return new FieldObject{ name, type };
}

FieldType Db::prevedNaFieldType(std::string type)
{
	FieldType fieldType;
	if (type.compare("string") == 0)
	{
		fieldType = FieldType::String;
	}
	else if (type.compare("int") == 0)
	{
		fieldType = FieldType::Integer;
	}
	else if (type.compare("double") == 0)
	{
		fieldType = FieldType::Double;
	}
	return fieldType;
}

std::string Db::prevedNaString(FieldType type)
{
	string str;
	switch (type)
	{
	case FieldType::String:
		str = "string";
		break;
	case FieldType::Integer:
		str = "int";
		break;
	case FieldType::Double:
		str = "double";
		break;
	default:
		break;
	}
	return str;
}
