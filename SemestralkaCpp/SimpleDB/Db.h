#ifndef __DB_H
#define __DB_H

#include "platform.h"
#include "helpful.h"
#include "Table.h"
#include "DoubleObject.h"
#include "IntObject.h"
#include "StringObject.h"
#include <vector>

// Datab�ze
class DLL_SPEC Db {
private:
	int pocetTabulek;
	std::string jmeno;
	std::vector<std::string> aktivniTabulky;
	bool jeTabulkaAktivni(std::string jmeno);
public:
	Db(std::string jmeno);
	virtual ~Db();
	// Otev�e datab�zi
	static Db* open(std::string database);
	// Uzav�e datab�zi (dealokuje pam�ov� prost�edky)
	void close();

	// Vytvo�� novou tabulku
	Table* createTable(std::string name, int fieldsCount, FieldObject** fields);
	// Otev�e existuj�c� tabulku
	Table* openTable(std::string name);
	// Otev�e tabulku (pokud neexistuje, vytvo�� automaticky novou)
	Table* openOrCreateTable(std::string name, int fieldsCount, FieldObject** fields);

	// Alokuje objekt �int�
	static Object* Int(int value);
	// Alokuje objekt �double�
	static Object* Double(double value);
	// Alokuje objekt �string�
	static Object* String(std::string value);
	// Alokuje objekt �field�
	static FieldObject* Field(std::string name, FieldType type);

	FieldType prevedNaFieldType(std::string type);
	std::string prevedNaString(FieldType type);
};

#endif
