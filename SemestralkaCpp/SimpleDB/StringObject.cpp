#include "StringObject.h"

std::string StringObject::getString() const
{
	return this->value;
}

void StringObject::setString(std::string value)
{
	this->value = value;
}

bool StringObject::isType(FieldType type) const
{
	if (type == FieldType::String)
	{
		return true;
	}
	else
	{
		return false;
	}
}
