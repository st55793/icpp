#include "Sit.h"

int Sit::DejNoveId()
{
	return 0;
}

void Sit::Pripoj(ASitovyPrvek* sitovyPrvek)
{
	if (!sitovePrvky.Obsahuje(sitovyPrvek))
	{
		sitovePrvky.Vloz(sitovyPrvek);
	}
}

void Sit::ProvadejVse()
{
	sitovePrvky.ZpracujPrvky([](ASitovyPrvek* sp) {
		sp->Provadej();
		}
	);
}
