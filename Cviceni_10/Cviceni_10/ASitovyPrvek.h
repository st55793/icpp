#ifndef __ASITOVYPRVEK_H
#define __ASITOVYPRVEK_H

#include "Fronta.h"
#include "ZpravaPort.h"

struct ASitovyPrvek {
protected:
	Fronta<ZpravaPort> prichoziZpravy;
	virtual void ZpracujPrichoziZpravu(ZpravaPort zp);
public:
	void VlozPrichoziZpravu(Zprava* zprava, ASitovyPrvek* port);
	virtual void Provadej();
	virtual void Pripoj(ASitovyPrvek* sitovyPrvek);
};
#endif
