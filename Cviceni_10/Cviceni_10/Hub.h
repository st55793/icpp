#ifndef __HUB_H
#define __HUB_H

#include "Fronta.h"
#include "ASitovyPrvek.h"

struct Hub : ASitovyPrvek {
private:
	ASitovyPrvek** pripojenePrvky;
	int maximumPripojenychPrvku;
	Fronta<Zprava*> zpracovaneZpravy;
	virtual void ZpracujPrichoziZpravu(ZpravaPort zp) override;
public:
	Hub(int pocetPortu);
	~Hub();
	virtual void Pripoj(ASitovyPrvek* sitovyPrvek) override;
	virtual void Provadej() override;
};

#endif
