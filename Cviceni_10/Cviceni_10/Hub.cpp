#include "Hub.h"

Hub::Hub(int pocetPortu)
{
	this->maximumPripojenychPrvku = pocetPortu;
	this->pripojenePrvky = new ASitovyPrvek *[this->maximumPripojenychPrvku];
	for (int i = 0; i < this->maximumPripojenychPrvku; i++)
	{
		this->pripojenePrvky[i] = nullptr;
	}
}

Hub::~Hub()
{
	for (int i = 0; i < this->maximumPripojenychPrvku; i++)
	{
		delete this->pripojenePrvky[i];
	}
	delete[] this->pripojenePrvky;
}

void Hub::Pripoj(ASitovyPrvek * sitovyPrvek)
{
	for (int i = 0; i < this->maximumPripojenychPrvku; i++)
	{
		if (!this->pripojenePrvky[i])
		{
			this->pripojenePrvky[i] = sitovyPrvek;
		}
	}
}

void Hub::Provadej()
{
	while (!prichoziZpravy.JePrazdna())
	{
		ZpravaPort zp = prichoziZpravy.Odeber();
		ZpracujPrichoziZpravu(zp);
	}

}

void Hub::ZpracujPrichoziZpravu(ZpravaPort zp)
{
	if (zpracovaneZpravy.Obsahuje(zp.zprava)) {
		return;
	}

	for (int i = 0; i < maximumPripojenychPrvku; i++)
	{
		if (pripojenePrvky[i] != nullptr)
		{
			if (pripojenePrvky[i] != zp.port)
			{
				pripojenePrvky[i]->VlozPrichoziZpravu(zp.zprava, this);
			}
		}
	}
	zpracovaneZpravy.Vloz(zp.zprava);

}
