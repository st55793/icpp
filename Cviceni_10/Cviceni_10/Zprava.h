#ifndef __ZPRAVA_H
#define __ZPRAVA_H

#include <string>

struct Zprava {
	int id;
	std::string adrZdroj;
	std::string adrCil;
	std::string obsah;

	Zprava() { }
	Zprava(int id, std::string adrZdroj, std::string adrCil, std::string obsah) :
		id(id), adrZdroj(adrZdroj), adrCil(adrCil), obsah(obsah) {
	}

};
#endif
