#ifndef __HRA_H
#define __HRA_H
#include "Objekt.h"
#include "PohyblivyObjekt.h"

class Hra
{
private:
	Objekt** objekty;
	int max = 10;
	int pocet = 0;
public:
	Hra();
	~Hra();
	void vlozObjekt(Objekt*);
	int* najdiIdStatickychObjektu(double, double, double, double);
	PohyblivyObjekt** najdiPohybliveObjektyVOblasti(double, double, double);
	PohyblivyObjekt** najdiPohybliveObjektyVOblasti(double, double, double, double, double);
};
#endif

