#ifndef __OBJEKT_H
#define __OBJEKT_H

class Objekt
{
private:
	int id;
	double x;
	double y;
public:
	Objekt(int);
	virtual ~Objekt();
	int getId() const;
	double getX() const;
	void setX(double);
	double getY() const;
	void setY(double);
};
#endif

