#include "Montrum.h"

int Montrum::getHp() const
{
	return this->hp;
}

void Montrum::setHp(int hp)
{
	this->hp = hp;
}

int Montrum::getMaxhp() const
{
	return this->maxhp;
}

void Montrum::setMaxhp(int maxhp)
{
	this->maxhp = maxhp;
}
