#ifndef __STATICKYOBJEKT_H
#define __STATICKYOBJEKT_H
#include "Objekt.h"
#include "TypPrekazky.h"

class StatickyObjekt : public Objekt
{
private:
	TypPrekazky typPrekazky;
public:
	StatickyObjekt(int, TypPrekazky);
	TypPrekazky getTypPrekazky() const;
};
#endif
